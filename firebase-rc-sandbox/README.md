# Getting started

1. Sign in to firebase, go to remote config section
2. Configure new parameter called "headerType" with two values: "classic", "modern"
3. Update remoteConfig variable in index.html
4. Open index.html in your browser, you'll see fetched headerType variable


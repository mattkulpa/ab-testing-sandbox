import { UnleashClient } from 'unleash-proxy-client';

// See all options in separate section.
const unleash = new UnleashClient({
    url: 'http://localhost:3000/proxy',
    clientKey: 'some-secret',
    appName: 'default',
    environment: 'development'
});
unleash.start();

unleash.on('ready', () => {
    const toggles = unleash.getAllToggles()
    const pre = document.createElement('pre')
    pre.innerHTML = JSON.stringify(toggles, null, 2)
    document.body.appendChild(pre)
  })

  


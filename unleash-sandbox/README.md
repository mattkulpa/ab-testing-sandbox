# Getting started
1. Open terminal go to /server directory
2. Run docker-compose command
```sh
docker-compose up -d
```
3. Open http://localhost:4242/login
4. Login with credentials
```
admin
unleash4all
```
5. Go to configure -> API access and create new token. Pick development environment and copy token.
6. Update UNLEASH_API_TOKEN in docker-compose with copied token
7. Run docker-compose again to recreate proxy server
```sh
docker-compose up -d
```
8. Create new toggle feature called "headerType" with type "experiment"
9. Add two variants: "classic" and "modern"
10. Create new strategy "standard" and enable feature toggle on development environment
11. Run client app
```
npm i
npx parcel index.html
```
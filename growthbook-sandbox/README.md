# Getting started

1. Run containers with GrowthBook and mongo database

```
docker-compose up -d
```

2. Open http://localhost:3000 and create account
3. Create metrics, experiment and API KEY
4. Replace API Key in app/index.html
5. Open app/index.html, and you'll join to one of experiment


